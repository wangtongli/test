.class Lcom/android/internal/policy/impl/HtcPhoneWindowManagerExtention;
.super Ljava/lang/Object;
.source "HtcPhoneWindowManagerExtention.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/policy/impl/HtcPhoneWindowManagerExtention$LogKeyCodeHandle;,
        Lcom/android/internal/policy/impl/HtcPhoneWindowManagerExtention$PhoneCallHandle;
    }
.end annotation


# static fields
#the value of this static final field might be set in the static constructor
.field private static final DEBUG_ON:Z = false

.field private static final TAG:Ljava/lang/String; = "HtcPhoneWindowManagerExtention"


# instance fields
.field public keyEventHandle:Lcom/android/internal/policy/impl/HtcKeyEventHandler;

.field public logKeyCodeHandle:Lcom/android/internal/policy/impl/HtcPhoneWindowManagerExtention$LogKeyCodeHandle;

.field private mInitialized:Z

.field private mPwmUtil:Lcom/android/internal/policy/impl/PhoneWindowManager$PWMUtil;

.field private mUtils:Lcom/android/internal/policy/impl/HtcPolicyUtils;

.field public phoneCallHandle:Lcom/android/internal/policy/impl/HtcPhoneWindowManagerExtention$PhoneCallHandle;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget-boolean v0, Lcom/htc/htcjavaflag/HtcBuildFlag;->Htc_DEBUG_flag:Z

    sput-boolean v0, Lcom/android/internal/policy/impl/HtcPhoneWindowManagerExtention;->DEBUG_ON:Z

    return-void
.end method

.method constructor <init>()V
    .locals 1

    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/internal/policy/impl/HtcPhoneWindowManagerExtention;->mInitialized:Z

    new-instance v0, Lcom/android/internal/policy/impl/HtcPolicyUtils;

    invoke-direct {v0}, Lcom/android/internal/policy/impl/HtcPolicyUtils;-><init>()V

    iput-object v0, p0, Lcom/android/internal/policy/impl/HtcPhoneWindowManagerExtention;->mUtils:Lcom/android/internal/policy/impl/HtcPolicyUtils;

    new-instance v0, Lcom/android/internal/policy/impl/HtcPhoneWindowManagerExtention$PhoneCallHandle;

    invoke-direct {v0, p0}, Lcom/android/internal/policy/impl/HtcPhoneWindowManagerExtention$PhoneCallHandle;-><init>(Lcom/android/internal/policy/impl/HtcPhoneWindowManagerExtention;)V

    iput-object v0, p0, Lcom/android/internal/policy/impl/HtcPhoneWindowManagerExtention;->phoneCallHandle:Lcom/android/internal/policy/impl/HtcPhoneWindowManagerExtention$PhoneCallHandle;

    new-instance v0, Lcom/android/internal/policy/impl/HtcPhoneWindowManagerExtention$LogKeyCodeHandle;

    invoke-direct {v0, p0}, Lcom/android/internal/policy/impl/HtcPhoneWindowManagerExtention$LogKeyCodeHandle;-><init>(Lcom/android/internal/policy/impl/HtcPhoneWindowManagerExtention;)V

    iput-object v0, p0, Lcom/android/internal/policy/impl/HtcPhoneWindowManagerExtention;->logKeyCodeHandle:Lcom/android/internal/policy/impl/HtcPhoneWindowManagerExtention$LogKeyCodeHandle;

    new-instance v0, Lcom/android/internal/policy/impl/HtcKeyEventHandler;

    invoke-direct {v0}, Lcom/android/internal/policy/impl/HtcKeyEventHandler;-><init>()V

    iput-object v0, p0, Lcom/android/internal/policy/impl/HtcPhoneWindowManagerExtention;->keyEventHandle:Lcom/android/internal/policy/impl/HtcKeyEventHandler;

    return-void
.end method

.method static synthetic access$000(Lcom/android/internal/policy/impl/HtcPhoneWindowManagerExtention;)Lcom/android/internal/policy/impl/HtcPolicyUtils;
    .locals 1

    iget-object v0, p0, Lcom/android/internal/policy/impl/HtcPhoneWindowManagerExtention;->mUtils:Lcom/android/internal/policy/impl/HtcPolicyUtils;

    return-object v0
.end method

.method static synthetic access$100()Z
    .locals 1

    sget-boolean v0, Lcom/android/internal/policy/impl/HtcPhoneWindowManagerExtention;->DEBUG_ON:Z

    return v0
.end method


# virtual methods
.method public init(Lcom/android/internal/policy/impl/PhoneWindowManager$PWMUtil;)V
    .locals 2

    iput-object p1, p0, Lcom/android/internal/policy/impl/HtcPhoneWindowManagerExtention;->mPwmUtil:Lcom/android/internal/policy/impl/PhoneWindowManager$PWMUtil;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/internal/policy/impl/HtcPhoneWindowManagerExtention;->mInitialized:Z

    const-string v0, "HtcPhoneWindowManagerExtention"

    const-string v1, "initialized"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/internal/policy/impl/HtcPhoneWindowManagerExtention;->keyEventHandle:Lcom/android/internal/policy/impl/HtcKeyEventHandler;

    invoke-virtual {v0, p1}, Lcom/android/internal/policy/impl/HtcKeyEventHandler;->init(Lcom/android/internal/policy/impl/PhoneWindowManager$PWMUtil;)V

    return-void
.end method

.method isPhoneInCall()Z
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/internal/policy/impl/HtcPhoneWindowManagerExtention;->mUtils:Lcom/android/internal/policy/impl/HtcPolicyUtils;

    iget-object v1, v1, Lcom/android/internal/policy/impl/HtcPolicyUtils;->phone:Lcom/android/internal/policy/impl/HtcPolicyUtils$PhoneUtil;

    invoke-virtual {v1}, Lcom/android/internal/policy/impl/HtcPolicyUtils$PhoneUtil;->isInCall()Z

    move-result v0

    return v0
.end method

.method isWakeKeyHandledByInputManager(ZI)Z
    .locals 2

    const/4 v0, 0x0

    if-nez p1, :cond_0

    const/16 v1, 0x1a

    if-ne p2, v1, :cond_0

    iget-object v1, p0, Lcom/android/internal/policy/impl/HtcPhoneWindowManagerExtention;->mUtils:Lcom/android/internal/policy/impl/HtcPolicyUtils;

    iget-object v1, v1, Lcom/android/internal/policy/impl/HtcPolicyUtils;->phone:Lcom/android/internal/policy/impl/HtcPolicyUtils$PhoneUtil;

    invoke-virtual {v1}, Lcom/android/internal/policy/impl/HtcPolicyUtils$PhoneUtil;->isInCall()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public resultCamcorderWakeKey(II)I
    .locals 0

    return p1
.end method
