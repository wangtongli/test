.class public Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;
.super Ljava/lang/Object;
.source "HtcKeyguardViewMediator.java"


# static fields
.field static final DEBUG:Z = false

.field static final TAG:Ljava/lang/String; = "HtcKeyguardViewMediator"


# instance fields
.field mContext:Landroid/content/Context;

.field private mIsHtcLockScreenInstalled:Z

.field mKeyguardMediator:Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;

.field mObsoleteKeyguardMediator:Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/android/internal/policy/impl/PhoneWindowManager;)V
    .locals 6

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v5, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mIsHtcLockScreenInstalled:Z

    iput-object p1, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mContext:Landroid/content/Context;

    :try_start_0
    iget-object v1, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const-string v2, "com.htc.lockscreen"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mIsHtcLockScreenInstalled:Z
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-boolean v1, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mIsHtcLockScreenInstalled:Z

    if-eqz v1, :cond_0

    const-string v1, "HtcKeyguardViewMediator"

    const-string v2, "HtcLockScreen is installed"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v1, Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;

    invoke-direct {v1, p1, p2}, Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;-><init>(Landroid/content/Context;Lcom/android/internal/policy/impl/PhoneWindowManager;)V

    iput-object v1, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mObsoleteKeyguardMediator:Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;

    :goto_1
    return-void

    :catch_0
    move-exception v0

    const-string v1, "HtcKeyguardViewMediator"

    const-string v2, "Package %s is not installed"

    new-array v3, v4, [Ljava/lang/Object;

    const-string v4, "com.htc.lockscreen"

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_0
    const-string v1, "HtcKeyguardViewMediator"

    const-string v2, "HtcLockScreen is not installed"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v1, Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;

    const/4 v2, 0x0

    invoke-direct {v1, p1, v2}, Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;-><init>(Landroid/content/Context;Lcom/android/internal/widget/LockPatternUtils;)V

    iput-object v1, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mKeyguardMediator:Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;

    goto :goto_1
.end method


# virtual methods
.method public clearIdleScreen()V
    .locals 1

    iget-boolean v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mIsHtcLockScreenInstalled:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mObsoleteKeyguardMediator:Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mObsoleteKeyguardMediator:Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;

    invoke-virtual {v0}, Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;->clearIdleScreen()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mKeyguardMediator:Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;

    if-eqz v0, :cond_0

    goto :goto_0
.end method

.method public dismiss()V
    .locals 1

    iget-boolean v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mIsHtcLockScreenInstalled:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mObsoleteKeyguardMediator:Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mObsoleteKeyguardMediator:Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;

    invoke-virtual {v0}, Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;->dismiss()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mKeyguardMediator:Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mKeyguardMediator:Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;

    invoke-virtual {v0}, Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;->dismiss()V

    goto :goto_0
.end method

.method public doKeyguardTimeout(Landroid/os/Bundle;)V
    .locals 1

    iget-boolean v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mIsHtcLockScreenInstalled:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mObsoleteKeyguardMediator:Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mObsoleteKeyguardMediator:Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;

    invoke-virtual {v0, p1}, Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;->doKeyguardTimeout(Landroid/os/Bundle;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mKeyguardMediator:Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mKeyguardMediator:Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;

    invoke-virtual {v0, p1}, Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;->doKeyguardTimeout(Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public forcePasswordTimeout()V
    .locals 1

    iget-boolean v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mIsHtcLockScreenInstalled:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mObsoleteKeyguardMediator:Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mObsoleteKeyguardMediator:Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;

    invoke-virtual {v0}, Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;->forcePasswordTimeout()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mKeyguardMediator:Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;

    if-eqz v0, :cond_0

    goto :goto_0
.end method

.method public getIdleScreenLabel()Ljava/lang/String;
    .locals 3

    const/4 v0, 0x0

    iget-boolean v1, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mIsHtcLockScreenInstalled:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mObsoleteKeyguardMediator:Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;

    if-eqz v1, :cond_1

    iget-object v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mObsoleteKeyguardMediator:Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;

    invoke-virtual {v0}, Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;->getIdleScreenLabel()Ljava/lang/String;

    move-result-object v0

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    iget-object v1, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mKeyguardMediator:Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;

    if-nez v1, :cond_0

    const-string v1, "HtcKeyguardViewMediator"

    const-string v2, "Keyguard is null in getIdleScreenLabel()"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public getKeyboardFlyHeight()I
    .locals 3

    const/4 v0, 0x0

    iget-boolean v1, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mIsHtcLockScreenInstalled:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mObsoleteKeyguardMediator:Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;

    if-eqz v1, :cond_1

    iget-object v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mObsoleteKeyguardMediator:Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;

    invoke-virtual {v0}, Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;->getKeyboardFlyHeight()I

    move-result v0

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mKeyguardMediator:Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;

    if-nez v1, :cond_0

    const-string v1, "HtcKeyguardViewMediator"

    const-string v2, "Keyguard is null in getKeyboardFlyHeight()"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public getNeedToShowKeyguard(Z)Z
    .locals 3

    const/4 v0, 0x0

    iget-boolean v1, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mIsHtcLockScreenInstalled:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mObsoleteKeyguardMediator:Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;

    if-eqz v1, :cond_1

    iget-object v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mObsoleteKeyguardMediator:Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;

    invoke-virtual {v0, p1}, Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;->getNeedToShowKeyguard(Z)Z

    move-result v0

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mKeyguardMediator:Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;

    if-nez v1, :cond_0

    const-string v1, "HtcKeyguardViewMediator"

    const-string v2, "Keyguard is null in getNeedToShowKeyguard()"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public hideIdleScreen()V
    .locals 1

    iget-boolean v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mIsHtcLockScreenInstalled:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mObsoleteKeyguardMediator:Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mObsoleteKeyguardMediator:Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;

    invoke-virtual {v0}, Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;->hideIdleScreen()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mKeyguardMediator:Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;

    if-eqz v0, :cond_0

    goto :goto_0
.end method

.method public isDismissable()Z
    .locals 2

    iget-boolean v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mIsHtcLockScreenInstalled:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mObsoleteKeyguardMediator:Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mObsoleteKeyguardMediator:Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;

    invoke-virtual {v0}, Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;->isDismissable()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mKeyguardMediator:Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mKeyguardMediator:Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;

    invoke-virtual {v0}, Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;->isDismissable()Z

    move-result v0

    goto :goto_0

    :cond_1
    const-string v0, "HtcKeyguardViewMediator"

    const-string v1, "Keyguard is null in isDismissable()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isHtcLockScreenInstalled()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mIsHtcLockScreenInstalled:Z

    return v0
.end method

.method public isInputRestricted()Z
    .locals 2

    iget-boolean v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mIsHtcLockScreenInstalled:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mObsoleteKeyguardMediator:Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mObsoleteKeyguardMediator:Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;

    invoke-virtual {v0}, Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;->isInputRestricted()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mKeyguardMediator:Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mKeyguardMediator:Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;

    invoke-virtual {v0}, Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;->isInputRestricted()Z

    move-result v0

    goto :goto_0

    :cond_1
    const-string v0, "HtcKeyguardViewMediator"

    const-string v1, "Keyguard is null in isInputRestricted()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isKeyboardFly()Z
    .locals 3

    const/4 v0, 0x0

    iget-boolean v1, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mIsHtcLockScreenInstalled:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mObsoleteKeyguardMediator:Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;

    if-eqz v1, :cond_1

    iget-object v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mObsoleteKeyguardMediator:Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;

    invoke-virtual {v0}, Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;->isKeyboardFly()Z

    move-result v0

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mKeyguardMediator:Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;

    if-nez v1, :cond_0

    const-string v1, "HtcKeyguardViewMediator"

    const-string v2, "Keyguard is null in isKeyboardFly()"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public isLockScreen()Z
    .locals 3

    const/4 v0, 0x0

    iget-boolean v1, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mIsHtcLockScreenInstalled:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mObsoleteKeyguardMediator:Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;

    if-eqz v1, :cond_1

    iget-object v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mObsoleteKeyguardMediator:Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;

    invoke-virtual {v0}, Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;->isLockScreen()Z

    move-result v0

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mKeyguardMediator:Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;

    if-nez v1, :cond_0

    const-string v1, "HtcKeyguardViewMediator"

    const-string v2, "Keyguard is null in isLockScreen()"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public isScreenOn()Z
    .locals 2

    iget-boolean v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mIsHtcLockScreenInstalled:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mObsoleteKeyguardMediator:Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mObsoleteKeyguardMediator:Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;

    invoke-virtual {v0}, Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;->isScreenOn()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mKeyguardMediator:Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mKeyguardMediator:Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;

    invoke-virtual {v0}, Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;->isScreenOn()Z

    move-result v0

    goto :goto_0

    :cond_1
    const-string v0, "HtcKeyguardViewMediator"

    const-string v1, "Keyguard is null in isScreenOn()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSecure()Z
    .locals 2

    iget-boolean v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mIsHtcLockScreenInstalled:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mObsoleteKeyguardMediator:Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mObsoleteKeyguardMediator:Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;

    invoke-virtual {v0}, Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;->isSecure()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mKeyguardMediator:Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mKeyguardMediator:Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;

    invoke-virtual {v0}, Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;->isSecure()Z

    move-result v0

    goto :goto_0

    :cond_1
    const-string v0, "HtcKeyguardViewMediator"

    const-string v1, "Keyguard is null in isSecure()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isShowing()Z
    .locals 2

    iget-boolean v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mIsHtcLockScreenInstalled:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mObsoleteKeyguardMediator:Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mObsoleteKeyguardMediator:Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;

    invoke-virtual {v0}, Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;->isShowing()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mKeyguardMediator:Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mKeyguardMediator:Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;

    invoke-virtual {v0}, Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;->isShowing()Z

    move-result v0

    goto :goto_0

    :cond_1
    const-string v0, "HtcKeyguardViewMediator"

    const-string v1, "Keyguard is null in isShowing()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isShowingAndNotHidden()Z
    .locals 2

    iget-boolean v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mIsHtcLockScreenInstalled:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mObsoleteKeyguardMediator:Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mObsoleteKeyguardMediator:Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;

    invoke-virtual {v0}, Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;->isShowingAndNotHidden()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mKeyguardMediator:Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mKeyguardMediator:Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;

    invoke-virtual {v0}, Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;->isShowingAndNotHidden()Z

    move-result v0

    goto :goto_0

    :cond_1
    const-string v0, "HtcKeyguardViewMediator"

    const-string v1, "Keyguard is null in isShowingAndNotHidden()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public keyguardDone(ZZ)V
    .locals 1

    iget-boolean v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mIsHtcLockScreenInstalled:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mObsoleteKeyguardMediator:Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mObsoleteKeyguardMediator:Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;

    invoke-virtual {v0, p1, p2}, Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;->keyguardDone(ZZ)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mKeyguardMediator:Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mKeyguardMediator:Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;

    invoke-virtual {v0, p1, p2}, Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;->keyguardDone(ZZ)V

    goto :goto_0
.end method

.method public notifyAfterCameraLaunch()V
    .locals 1

    iget-boolean v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mIsHtcLockScreenInstalled:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mObsoleteKeyguardMediator:Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mObsoleteKeyguardMediator:Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;

    invoke-virtual {v0}, Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;->notifyAfterCameraLaunch()V

    :cond_0
    return-void
.end method

.method public notifyFullScreenWindowName(Ljava/lang/String;)V
    .locals 1

    iget-boolean v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mIsHtcLockScreenInstalled:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mObsoleteKeyguardMediator:Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mObsoleteKeyguardMediator:Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;

    invoke-virtual {v0, p1}, Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;->notifyFullScreenWindowName(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public onDreamingStarted()V
    .locals 1

    iget-boolean v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mIsHtcLockScreenInstalled:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mObsoleteKeyguardMediator:Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mObsoleteKeyguardMediator:Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;

    invoke-virtual {v0}, Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;->onDreamingStarted()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mKeyguardMediator:Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mKeyguardMediator:Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;

    invoke-virtual {v0}, Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;->onDreamingStarted()V

    goto :goto_0
.end method

.method public onDreamingStopped()V
    .locals 1

    iget-boolean v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mIsHtcLockScreenInstalled:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mObsoleteKeyguardMediator:Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mObsoleteKeyguardMediator:Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;

    invoke-virtual {v0}, Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;->onDreamingStopped()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mKeyguardMediator:Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mKeyguardMediator:Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;

    invoke-virtual {v0}, Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;->onDreamingStopped()V

    goto :goto_0
.end method

.method public onScreenTurnedOff(I)V
    .locals 1

    iget-boolean v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mIsHtcLockScreenInstalled:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mObsoleteKeyguardMediator:Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mObsoleteKeyguardMediator:Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;

    invoke-virtual {v0, p1}, Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;->onScreenTurnedOff(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mKeyguardMediator:Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mKeyguardMediator:Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;

    invoke-virtual {v0, p1}, Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;->onScreenTurnedOff(I)V

    goto :goto_0
.end method

.method public onScreenTurnedOn(Lcom/android/internal/policy/impl/KeyguardViewManager$ShowListener;)V
    .locals 1

    iget-object v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mKeyguardMediator:Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mKeyguardMediator:Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;

    invoke-virtual {v0, p1}, Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;->onScreenTurnedOn(Lcom/android/internal/policy/impl/KeyguardViewManager$ShowListener;)V

    :cond_0
    return-void
.end method

.method public onScreenTurnedOnObsolete(Lcom/android/internal/policy/impl/KeyguardViewManager$ShowListener;)V
    .locals 1

    iget-object v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mObsoleteKeyguardMediator:Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mObsoleteKeyguardMediator:Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;

    invoke-virtual {v0, p1}, Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;->onScreenTurnedOn(Lcom/android/internal/policy/impl/KeyguardViewManager$ShowListener;)V

    :cond_0
    return-void
.end method

.method public onSystemReady()V
    .locals 1

    iget-boolean v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mIsHtcLockScreenInstalled:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mObsoleteKeyguardMediator:Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mObsoleteKeyguardMediator:Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;

    invoke-virtual {v0}, Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;->onSystemReady()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mKeyguardMediator:Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mKeyguardMediator:Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;

    invoke-virtual {v0}, Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;->onSystemReady()V

    goto :goto_0
.end method

.method public onWakeKeyWhenKeyguardShowingTq(I)V
    .locals 1

    iget-boolean v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mIsHtcLockScreenInstalled:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mObsoleteKeyguardMediator:Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mObsoleteKeyguardMediator:Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;

    invoke-virtual {v0, p1}, Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;->onWakeKeyWhenKeyguardShowingTq(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mKeyguardMediator:Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mKeyguardMediator:Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;

    invoke-virtual {v0, p1}, Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;->onWakeKeyWhenKeyguardShowingTq(I)V

    goto :goto_0
.end method

.method public onWakeMotionWhenKeyguardShowingTq()V
    .locals 1

    iget-boolean v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mIsHtcLockScreenInstalled:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mObsoleteKeyguardMediator:Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mObsoleteKeyguardMediator:Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;

    invoke-virtual {v0}, Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;->onWakeMotionWhenKeyguardShowingTq()Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mKeyguardMediator:Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mKeyguardMediator:Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;

    invoke-virtual {v0}, Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;->onWakeMotionWhenKeyguardShowingTq()V

    goto :goto_0
.end method

.method public setCurrentUser(I)V
    .locals 1

    iget-boolean v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mIsHtcLockScreenInstalled:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mObsoleteKeyguardMediator:Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mObsoleteKeyguardMediator:Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;

    invoke-virtual {v0, p1}, Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;->setCurrentUser(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mKeyguardMediator:Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mKeyguardMediator:Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;

    invoke-virtual {v0, p1}, Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;->setCurrentUser(I)V

    goto :goto_0
.end method

.method public setHidden(Z)V
    .locals 1

    iget-boolean v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mIsHtcLockScreenInstalled:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mObsoleteKeyguardMediator:Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mObsoleteKeyguardMediator:Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;

    invoke-virtual {v0, p1}, Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;->setHidden(Z)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mKeyguardMediator:Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mKeyguardMediator:Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;

    invoke-virtual {v0, p1}, Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;->setHidden(Z)V

    goto :goto_0
.end method

.method public setIdleScreen(Landroid/content/ComponentName;)V
    .locals 1

    iget-boolean v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mIsHtcLockScreenInstalled:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mObsoleteKeyguardMediator:Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mObsoleteKeyguardMediator:Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;

    invoke-virtual {v0, p1}, Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;->setIdleScreen(Landroid/content/ComponentName;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mKeyguardMediator:Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;

    if-eqz v0, :cond_0

    goto :goto_0
.end method

.method public setKeyguardEnabled(Z)V
    .locals 1

    iget-boolean v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mIsHtcLockScreenInstalled:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mObsoleteKeyguardMediator:Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mObsoleteKeyguardMediator:Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;

    invoke-virtual {v0, p1}, Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;->setKeyguardEnabled(Z)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mKeyguardMediator:Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mKeyguardMediator:Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;

    invoke-virtual {v0, p1}, Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;->setKeyguardEnabled(Z)V

    goto :goto_0
.end method

.method public setLastUserActivity()V
    .locals 1

    iget-boolean v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mIsHtcLockScreenInstalled:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mObsoleteKeyguardMediator:Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mObsoleteKeyguardMediator:Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;

    invoke-virtual {v0}, Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;->setLastUserActivity()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mKeyguardMediator:Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;

    if-eqz v0, :cond_0

    goto :goto_0
.end method

.method public setNeedRedraw(Z)V
    .locals 1

    iget-boolean v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mIsHtcLockScreenInstalled:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mObsoleteKeyguardMediator:Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mObsoleteKeyguardMediator:Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;

    invoke-virtual {v0, p1}, Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;->setNeedRedraw(Z)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mKeyguardMediator:Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;

    if-eqz v0, :cond_0

    goto :goto_0
.end method

.method public setNeedToShowKeyguard(Z)V
    .locals 1

    iget-boolean v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mIsHtcLockScreenInstalled:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mObsoleteKeyguardMediator:Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mObsoleteKeyguardMediator:Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;

    invoke-virtual {v0, p1}, Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;->setNeedToShowKeyguard(Z)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mKeyguardMediator:Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;

    if-eqz v0, :cond_0

    goto :goto_0
.end method

.method public setNeedToShowKeyguardAnimation(Z)V
    .locals 1

    iget-boolean v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mIsHtcLockScreenInstalled:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mObsoleteKeyguardMediator:Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mObsoleteKeyguardMediator:Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;

    invoke-virtual {v0, p1}, Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;->setNeedToShowKeyguardAnimation(Z)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mKeyguardMediator:Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;

    if-eqz v0, :cond_0

    goto :goto_0
.end method

.method public setTransparent(Z)V
    .locals 1

    iget-boolean v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mIsHtcLockScreenInstalled:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mObsoleteKeyguardMediator:Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mObsoleteKeyguardMediator:Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;

    invoke-virtual {v0, p1}, Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;->setTransparent(Z)V

    :cond_0
    return-void
.end method

.method public showAssistant()V
    .locals 1

    iget-boolean v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mIsHtcLockScreenInstalled:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mObsoleteKeyguardMediator:Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mObsoleteKeyguardMediator:Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;

    invoke-virtual {v0}, Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;->showAssistant()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mKeyguardMediator:Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mKeyguardMediator:Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;

    invoke-virtual {v0}, Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;->showAssistant()V

    goto :goto_0
.end method

.method public showIdleScreen()V
    .locals 1

    iget-boolean v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mIsHtcLockScreenInstalled:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mObsoleteKeyguardMediator:Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mObsoleteKeyguardMediator:Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;

    invoke-virtual {v0}, Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;->showIdleScreen()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mKeyguardMediator:Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;

    if-eqz v0, :cond_0

    goto :goto_0
.end method

.method public showKeyguard(Z)V
    .locals 1

    iget-boolean v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mIsHtcLockScreenInstalled:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mObsoleteKeyguardMediator:Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mObsoleteKeyguardMediator:Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;

    invoke-virtual {v0, p1}, Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;->showKeyguard(Z)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mKeyguardMediator:Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;

    if-eqz v0, :cond_0

    goto :goto_0
.end method

.method public userActivity()V
    .locals 1

    iget-boolean v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mIsHtcLockScreenInstalled:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mObsoleteKeyguardMediator:Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mObsoleteKeyguardMediator:Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;

    invoke-virtual {v0}, Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;->userActivity()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mKeyguardMediator:Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mKeyguardMediator:Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;

    invoke-virtual {v0}, Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;->userActivity()V

    goto :goto_0
.end method

.method public verifyUnlock(Landroid/view/WindowManagerPolicy$OnKeyguardExitResult;)V
    .locals 1

    iget-boolean v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mIsHtcLockScreenInstalled:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mObsoleteKeyguardMediator:Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mObsoleteKeyguardMediator:Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;

    invoke-virtual {v0, p1}, Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;->verifyUnlock(Landroid/view/WindowManagerPolicy$OnKeyguardExitResult;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mKeyguardMediator:Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/internal/policy/impl/HtcKeyguardViewMediator;->mKeyguardMediator:Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;

    invoke-virtual {v0, p1}, Lcom/android/internal/policy/impl/MiuiKeyguardViewMediator;->verifyUnlock(Landroid/view/WindowManagerPolicy$OnKeyguardExitResult;)V

    goto :goto_0
.end method
