.class public Lcom/android/internal/policy/impl/KeyguardUpdateMonitorCallback;
.super Ljava/lang/Object;
.source "KeyguardUpdateMonitorCallback.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method onClockVisibilityChanged()V
    .locals 0

    return-void
.end method

.method onDeviceLockChange(Z)V
    .locals 0

    return-void
.end method

.method onDevicePolicyManagerStateChanged()V
    .locals 0

    return-void
.end method

.method onDeviceProvisioned()V
    .locals 0

    return-void
.end method

.method onPhoneStateChanged(I)V
    .locals 0

    return-void
.end method

.method onRefreshBatteryInfo(Lcom/android/internal/policy/impl/KeyguardUpdateMonitor$BatteryStatus;)V
    .locals 0

    return-void
.end method

.method onRefreshCarrierInfo(ILjava/lang/CharSequence;Ljava/lang/CharSequence;)V
    .locals 0

    return-void
.end method

.method onRingerModeChanged(I)V
    .locals 0

    return-void
.end method

.method onSimStateChanged(Lcom/android/internal/telephony/IccCardConstants$State;)V
    .locals 0

    return-void
.end method

.method onTimeChanged()V
    .locals 0

    return-void
.end method

.method onUserRemoved(I)V
    .locals 0

    return-void
.end method

.method onUserSwitched(I)V
    .locals 0

    return-void
.end method
